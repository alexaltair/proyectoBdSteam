<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@page import="mipk.beanDB"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Steam</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="top-number"><p> PROYECTO BD ---> Alejandro Avalos Moreno </p></div>
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="https://www.facebook.com/Steam/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/steam_games?lang=es"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Index.jsp"><img src="images/logo.jpg" widht="100" height="80" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="Index.jsp">Inicio</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultas Usuarios <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="general.jsp">Info Usuarios</a></li>
                                <li><a href="ListaNumJuegosUsu.jsp">Usuario Num Juegos</a></li>
                                <li><a href="UsuariosCreadosAnios.jsp">Info Usuarios/a�o</a></li>
                                <li><a href="Nivel.jsp">Info seg�n nivel</a></li>
                                <li><a href="NumAmigosGrupos.jsp">Amigos/Grupos Usuarios</a></li>
                                <li><a href="InsertUsuarios.jsp">Crear Usuarios</a></li>
                                <li><a href="UpdateUsuarios.jsp">Actualizar Usuarios</a></li>
                                <li><a href="UsuariosGrupos.jsp">Grupos de usuarios</a></li>
                            </ul>
                        </li>
          
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultas Juegos <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="generalJuegos.jsp">Info juegos</a></li>
                                <li><a href="Buscador.jsp">Buscador</a></li>
                                <li><a href="Categorias.jsp">Categorias</a></li>
                                <li><a href="InsertJuegos.jsp">A�adir Juegos</a></li>
                                <li><a href="JuegosAnios.jsp">Juegos segun A�o</a></li>
                            </ul>
                        </li>
                                             
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
    <section id="services" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown">
                <h2>Creacion fichas juegos</h2>
                <p class="lead">Formulario que nos permite a�adir nuevos juegos a la bd</p>
            </div>
		    
		    
					<form role="form" action="InsertJuegos.jsp" method="get">
						<div class="form-group has-error">
							<label class="control-label" for="error">Nombre</label> <input
								type="text" class="form-control" id="error" name="nombre" 
								placeholder ="Introduzca el nombre del juego..." 
								/>
						</div>
						<div class="form-group has-error">
							<label class="control-label" for="error">Compa�ia</label> <input
								type="text" class="form-control" id="error" name="compania" 
								placeholder ="Introduzca la compa�ia del juego.." 
								/>
						</div>
						<div class="form-group has-error">
							<label class="control-label" for="error">Fecha Lanzamiento</label> <input
								type="text" class="form-control" id="error" name="fechaLanz" 
								placeholder ="Introduzca la fecha de lanzamiento(YY-MM-DD)..." 
								/>
						</div>
						
						<div class="form-group has-error">
							<label class="control-label" for="error">Idiomas</label> <input
								type="text" class="form-control" id="error" name="idiomas" 
								placeholder ="Introduzca los idiomas del juego(idioma1,idioma2,...)..." />
						</div>
						
						<div style="text-align: center; justify-content: center; display: flex;">	
							<input type="submit" value="A�adir Juego">
						</div>
						
					</form>
					<%
					    beanDB basededatos = new beanDB();
									
						String nombre = "";
						String compania ="";
					    String fechaLanz = "";
						String idiomas = "";
					
					
						try {
							nombre = request.getParameter("nombre").toString();
							compania = request.getParameter("compania").toString();
							fechaLanz = request.getParameter("fechaLanz").toString();
							idiomas = request.getParameter("idiomas").toString();
							
							

							String queryUpdate = "insert into juego (nombre, compania, fechaLanzamiento, idiomas) values ('"
									+ nombre + "','"+compania+"', '" + fechaLanz + "', '" + idiomas + "')";
							basededatos.insert(queryUpdate);
							%>

				</div>
				<div class="col-md-6" style="text-align: center; justify-content: center;">
						<%if (nombre.equals("") || compania.equals("") || fechaLanz.equals("") || idiomas.equals("")) {
					%>
						<label style="font-color:#a94442">No se pudo a�adir el juego</label>
					<%
						} else {
					%>
						<b>El juego se a�adio correctamente</b>
					<%
						}
						} catch (Exception e) {
						}
					%>
					</div>
         
        </div><!--/.container-->
    </section><!--/#services-->


    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2016 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">Steam (Alejandro Avalos Moreno)</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Ir arriba</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>


</body>
</html>