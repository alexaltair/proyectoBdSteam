<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@page import="mipk.beanDB"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Steam</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="top-number"><p> PROYECTO BD ---> Alejandro Avalos Moreno </p></div>
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="https://www.facebook.com/Steam/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/steam_games?lang=es"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Index.jsp"><img src="images/logo.jpg" widht="100" height="80" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="Index.jsp">Inicio</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultas Usuarios <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="general.jsp">Info Usuarios</a></li>
                                <li><a href="ListaNumJuegosUsu.jsp">Usuario Num Juegos</a></li>
                                <li><a href="UsuariosCreadosAnios.jsp">Info Usuarios/a�o</a></li>
                                <li><a href="Nivel.jsp">Info seg�n nivel</a></li>
                                <li><a href="NumAmigosGrupos.jsp">Amigos/Grupos Usuarios</a></li>
                                <li><a href="InsertUsuarios.jsp">Crear Usuarios</a></li>
                                <li><a href="UpdateUsuarios.jsp">Actualizar Usuarios</a></li>
                                <li><a href="UsuariosGrupos.jsp">Grupos de usuarios</a></li>
                            </ul>
                        </li>
          
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultas Juegos <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="generalJuegos.jsp">Info juegos</a></li>
                                <li><a href="Buscador.jsp">Buscador</a></li>
                                <li><a href="Categorias.jsp">Categorias</a></li>
                                <li><a href="InsertJuegos.jsp">A�adir Juegos</a></li>
                                <li><a href="JuegosAnios.jsp">Juegos segun A�o</a></li>
                            </ul>
                        </li>
                                             
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
    <section id="services" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown">
                <h2>Informe Juegos segun genero</h2>
                <p class="lead">Select que nos permite elegir el genero para visualizar la informacion de los juegos pertenecientes a ese genero.</p>
            </div>
					<% 
                    String query = "select idGenero, nombre from genero";
                    beanDB basededatos = new beanDB(); //
                    String [][] tablaConsultas = basededatos.resConsultaSelectA3(query);
                    %>
                    
                    
                    
                    <form name="genero" action="Categorias.jsp" method="get"> 
					<select name="generos" onchange="submit();">
						<option> Seleccione un genero... </option>
					   	<%for (int i = 0; i < tablaConsultas.length; i++){ %>
					   	
					   	
						<option value = <%= tablaConsultas[i][0] %>> <%= tablaConsultas [i][1]%> </option> 
						<% } %>
					
						
					</select>
					</form>
                    
                    <% 
                    String generos="";
                    try {
                         generos = request.getParameter("generos").toString();
                    } catch (Exception e){}
                    
                   
                    
                 
                    
                    String query2 ="select juegos.nombre,compania,fechaLanzamiento,idiomas,generos.nombre from juego juegos join juegos_has_generos on (juegos.idJuego=juegos_has_generos.idJuego) join genero generos on(juegos_has_generos.idGenero=generos.idGenero) where generos.idGenero = "+generos+" order by juegos.nombre desc; ";
					String [][] tablaConsultas2 = basededatos.resConsultaSelectA3(query2); 
					%>
					
					
					
					
                    
					<br/>
					<table border="1">
					<% if (tablaConsultas2!=null) {
					%> 
					<tr> 
						<th> Nombre </th>
						<th> Compa�ia </th>
						<th> Lanzamiento </th>
						<th> Idiomas </th>
						<th> Genero </th>
					</tr>
					<% for (int i = 0; i < tablaConsultas2.length; i++) {
					%><tr> <% for (int j = 0; j < tablaConsultas2[i].length; j++) {
					%><td> <%=tablaConsultas2[i][j] %> </td> <% 
					}%>
					</tr> <% 
					}
					}
					%>
					</table>
					
         
        </div><!--/.container-->
    </section><!--/#services-->


    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2016 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">Steam (Alejandro Avalos Moreno)</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Ir arriba</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>


</body>
</html>