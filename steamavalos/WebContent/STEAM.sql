DROP DATABASE steam;
CREATE DATABASE steam;
USE steam;

CREATE TABLE usuario (
idUsuario int PRIMARY KEY AUTO_INCREMENT,
nombreUsuario varchar(30) not null,
password varchar (10) not null,
nombreReal varchar(15) not null,
apellidos varchar(30) not null,
nivel int (3) not null, 
provincia varchar (20),
pais varchar (20),
bio varchar (200),
capturas blob, 
avatar blob,
fechaCreacion datetime,
CONSTRAINT noRepetirNomUsuario UNIQUE (nombreUsuario)
)ENGINE=InnoDB;

CREATE TABLE amigos (
idAmigo int PRIMARY KEY AUTO_INCREMENT,
idUsuario int not null,
nombreUsuario varchar (30) not null,
nivel int (3) not null, 
provincia varchar (30),
pais varchar (20),
bio varchar (200),
avatar blob,
CONSTRAINT fk_usuarioAmigos FOREIGN KEY (idUsuario) REFERENCES usuario (idUsuario)
)ENGINE=InnoDB;

CREATE TABLE grupo (
idGrupo int PRIMARY KEY AUTO_INCREMENT,
nombreGrupo varchar(60) not null,
CONSTRAINT noRepetirNomGrupo UNIQUE (nombreGrupo)
)ENGINE=InnoDB;

CREATE TABLE usuario_has_grupos (
idGrupo int,
idUsuario int,
CONSTRAINT pk_usuario_has_grupos PRIMARY KEY (idGrupo,idUsuario),
CONSTRAINT fk_UsuarioGrupo FOREIGN KEY (idUsuario) REFERENCES usuario (idUsuario),
CONSTRAINT fk_GrupoUsuario FOREIGN KEY (idGrupo) REFERENCES grupo (idGrupo)
)ENGINE=InnoDB;

CREATE TABLE conversaciones (
idConversacion int PRIMARY KEY AUTO_INCREMENT,
idUsuario1 int not null,
idUsuario2 int not null,
CONSTRAINT fk_usuario1Conversacion FOREIGN KEY (idUsuario1) REFERENCES usuario (idUsuario),
CONSTRAINT fk_usuario2Conversacion FOREIGN KEY (idUsuario2) REFERENCES usuario (idUsuario)
)ENGINE=InnoDB;

CREATE TABLE mensaje (
idMensaje int PRIMARY KEY AUTO_INCREMENT,
idUsuario int not null,
idConversacion int not null,
contenido varchar (200) not null, 
fechaHora datetime not null,
CONSTRAINT fk_UsuarioMensaje FOREIGN KEY (idUsuario) REFERENCES usuario (idUsuario),
CONSTRAINT fk_ConversacionMensaje FOREIGN KEY (idConversacion) REFERENCES conversaciones (idConversacion)
)ENGINE=InnoDB;

CREATE TABLE juego (
idJuego int PRIMARY KEY AUTO_INCREMENT, 
nombre varchar (60) not null,
compania varchar (80) not null, 
fechaLanzamiento date not null,
idiomas varchar (80) not null,
CONSTRAINT noRepetirNombreJuego UNIQUE (nombre)
)ENGINE=InnoDB;

CREATE TABLE usuario_has_juegos (
idUsuario int,
idJuego int,
fechaCompra datetime not null,
CONSTRAINT pk_usuario_has_juegos PRIMARY KEY (idUsuario,idJuego),
CONSTRAINT fkUsuarioJuego FOREIGN KEY (idUsuario) REFERENCES usuario (idUsuario),
CONSTRAINT fkJuegoUsuario FOREIGN KEY (idJuego) REFERENCES juego (idJuego)
)ENGINE=InnoDB;

CREATE TABLE genero (
idGenero int PRIMARY KEY AUTO_INCREMENT,
nombre varchar (20) not null,
CONSTRAINT noRepetirNomGenero UNIQUE (nombre)
)ENGINE=InnoDB;

CREATE TABLE juegos_has_generos (
idJuego int,
idGenero int, 
CONSTRAINT pk_juegos_has_generos PRIMARY KEY (idJuego,idGenero),
CONSTRAINT fk_juegoGenero FOREIGN KEY (idJuego) REFERENCES juego (idJuego),
CONSTRAINT fK_GeneroJuego FOREIGN KEY (idGenero) REFERENCES genero (idGenero)
)ENGINE=InnoDB;


/* INSERT TABLA USUARIO*/
INSERT usuario (idUsuario, nombreUsuario, password, nombreReal, apellidos, nivel, provincia, pais, bio, fechaCreacion) VALUES (1,'Alvaro1920','1234','Alvaro','Cuenca',30,'Valencia','España','Surf y amigos','2010-05-10 20:09:26');
INSERT usuario (idUsuario, nombreUsuario, password, nombreReal, apellidos, nivel, provincia, pais, fechaCreacion) VALUES (2,'Alejandro1993','3456','Alejandro','Avalos Moreno',100,'Sevilla','España','2009-07-14 21:12:24');
INSERT usuario (idUsuario, nombreUsuario, password, nombreReal, apellidos, nivel, provincia, pais, bio, fechaCreacion) VALUES (3,'Adriamond','abcd','Adrian','Sanchez',91,'Almeria','España','Informatico','2008-08-23 15:25:43');
INSERT usuario (idUsuario, nombreUsuario, password, nombreReal, apellidos, nivel, provincia, pais, fechaCreacion) VALUES (4,'peperpez','asdf','Pablo','Perez Lopez',50,'Madrid','España','2008-07-13 10:46:12');
INSERT usuario (idUsuario, nombreUsuario, password, nombreReal, apellidos, nivel, provincia, pais, fechaCreacion) VALUES (5,'Monti','qwerty','Jose Carlos','Montañez Lopez',300,'Barcelona','España','2009-01-20 09:02:04');
INSERT usuario (idUsuario, nombreUsuario, password, nombreReal, apellidos, nivel, provincia, pais, fechaCreacion) VALUES (6,'Natalie2000','zxcv','Natalie','Smith',200,'Liverpool','United Kingdom','2011-07-15 12:11:10');
INSERT usuario (idUsuario, nombreUsuario, password, nombreReal, apellidos, nivel, provincia, pais, fechaCreacion) VALUES (7,'Dylan1980','aaaa','Dylan','Rowland',101,'Dublin','Ireland','2012-03-16 07:01:08');

/* INSERT TABLA AMIGOS*/
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (1,2,'peperpez',50,'Madrid','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais, bio) VALUES (2,2,'adriamond',91,'Almeria','España','Informatico');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (3,2,'Monti',300,'Barcelona','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (4,1,'Alejandro1993',100,'Sevilla','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (5,3,'Alejandro1993',100,'Sevilla','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (6,3,'peperpez',50,'Madrid','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (7,4,'Monti',300,'Barcelona','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (8,4,'Alejandro1993',100,'Sevilla','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais, bio) VALUES (9,4,'adriamond',91,'Almeria','España','Informatico');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais, bio) VALUES (10,4,'Alvaro1920',30,'Valencia','España','Surf y amigos');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais, bio) VALUES (11,2,'Alvaro1920',30,'Valencia','España','Surf y amigos');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (12,1,'peperpez',50,'Madrid','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (13,5,'Alejandro1993',100,'Sevilla','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (14,5,'peperpez',50,'Madrid','España');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (15,6,'Dylan1980',101,'Dublin','Ireland');
INSERT amigos (idAmigo, idUsuario, nombreUsuario, nivel, provincia, pais) VALUES (16,7,'Natalie2000',200,'Liverpool','United Kingdom');

/* INSERT TABLA GRUPOS*/
INSERT grupo VALUES (1,'The Witcher 3');
INSERT grupo VALUES (2,'Musica Indie');
INSERT grupo VALUES (3,'Futbol en general');
INSERT grupo VALUES (4,'Juegos de Accion');
INSERT grupo VALUES (5,'Cine');
INSERT grupo VALUES (6,'Informatica');
INSERT grupo VALUES (7,'Manga/Anime');
INSERT grupo VALUES (8,'GTA');
INSERT grupo VALUES (9,'FIFA');
INSERT grupo VALUES (10,'Baloncesto');
INSERT grupo VALUES (11,'Electronica');
INSERT grupo VALUES (12,'Quedadas');

/* INSERT TABLA usuario_has_grupos */
INSERT usuario_has_grupos VALUES (1,2);
INSERT usuario_has_grupos VALUES (1,1);
INSERT usuario_has_grupos VALUES (1,3);
INSERT usuario_has_grupos VALUES (2,2);
INSERT usuario_has_grupos VALUES (2,6);
INSERT usuario_has_grupos VALUES (2,7);
INSERT usuario_has_grupos VALUES (3,2);
INSERT usuario_has_grupos VALUES (3,4);
INSERT usuario_has_grupos VALUES (3,5);
INSERT usuario_has_grupos VALUES (4,6);
INSERT usuario_has_grupos VALUES (4,7);
INSERT usuario_has_grupos VALUES (5,2);
INSERT usuario_has_grupos VALUES (6,2);
INSERT usuario_has_grupos VALUES (6,3);
INSERT usuario_has_grupos VALUES (6,4);
INSERT usuario_has_grupos VALUES (6,5);
INSERT usuario_has_grupos VALUES (6,7);
INSERT usuario_has_grupos VALUES (7,2);
INSERT usuario_has_grupos VALUES (7,4);
INSERT usuario_has_grupos VALUES (8,3);
INSERT usuario_has_grupos VALUES (8,1);
INSERT usuario_has_grupos VALUES (9,2);
INSERT usuario_has_grupos VALUES (9,4);
INSERT usuario_has_grupos VALUES (9,5);
INSERT usuario_has_grupos VALUES (9,1);
INSERT usuario_has_grupos VALUES (10,4);
INSERT usuario_has_grupos VALUES (10,5);
INSERT usuario_has_grupos VALUES (11,2);
INSERT usuario_has_grupos VALUES (11,5);
INSERT usuario_has_grupos VALUES (12,1);
INSERT usuario_has_grupos VALUES (12,2);
INSERT usuario_has_grupos VALUES (12,3);
INSERT usuario_has_grupos VALUES (12,4);
INSERT usuario_has_grupos VALUES (12,5);
INSERT usuario_has_grupos VALUES (12,6);
INSERT usuario_has_grupos VALUES (12,7);

/* INSERT TABLA CONVERSACIONES*/
INSERT conversaciones VALUES (1,2,3);
INSERT conversaciones VALUES (2,2,4);
INSERT conversaciones VALUES (3,1,5);
INSERT conversaciones VALUES (4,3,7);

/* INSERT TABLA MENSAJES */
INSERT mensaje VALUES (1,2,1,'Buenas, que tal?','2015-05-17 23:59:02');
INSERT mensaje VALUES (2,3,1,'Aqui jugando al Counter','2015-05-17 00:01:02');
INSERT mensaje VALUES (3,2,2,'Buenaaas','2015-04-10 20:50:02');
INSERT mensaje VALUES (4,4,2,'Hola tio','2015-04-17 20:51:00');
INSERT mensaje VALUES (5,1,3,'Buenos dias','2015-01-13 14:59:02');
INSERT mensaje VALUES (6,5,3,'Hola Alvaro','2015-01-13 15:00:08');
INSERT mensaje VALUES (7,3,4,'whats up Dylan?','2015-02-20 10:05:20');
INSERT mensaje VALUES (8,7,4,'Perfect, dude','2015-02-20 10:20:25');

/* INSERT TABLA JUEGOS */
INSERT juego VALUES (1,'The Witcher 3','CD Projekt','2015-05-19','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (2,'Grand Theft Auto V','Rockstar games','2013-09-17','Español, ingles, polaco, italiano');
INSERT juego VALUES (3,'FIFA 15','EA SPORTS','2014-09-23','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (4,'Fallout 4','Bethesda','2015-11-10','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (5,'Dota 2','Valve','2013-07-13','Español, ingles,frances, aleman');
INSERT juego VALUES (6,'Guitar Hero Live','Activision','2015-10-20','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (7,'Batman Arkham Knight','Rocksteady','2015-06-23','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (8,'Dragon Age: Inquisition','Bioware','2014-11-18','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (9,'Life is Strange','Dontnod','2015-01-29','Ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (10,'Borderlands 2','Gearbox','2012-09-18','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (11,'Portal 2','Valve','2013-04-18','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (12,'Tales from the borderlands','Telltale Games','2014-11-25 03:59:02','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (13,'Half Life 2','Valve','2004-11-16','Ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (14,'Civilization IV','Firaxis Games','2005-04-04','Español, ingles, polaco, italiano, frances, aleman');
INSERT juego VALUES (15,'Alan Wake','Remedy Entertainment','2010-05-14','Español, ingles, polaco, italiano, frances, aleman');

/* INSERT TABLA USUARIOS_HAS_JUEGOS */
INSERT usuario_has_juegos VALUES (2,1,'2015-09-13 23:59:02');
INSERT usuario_has_juegos VALUES (2,2,'2015-10-17 24:00:01');
INSERT usuario_has_juegos VALUES (2,3,'2015-12-18 10:11:12');
INSERT usuario_has_juegos VALUES (2,4,'2014-09-13 09:05:10');
INSERT usuario_has_juegos VALUES (2,5,'2015-05-11 08:09:10');
INSERT usuario_has_juegos VALUES (2,11,'2013-06-10 06:07:08');
INSERT usuario_has_juegos VALUES (2,14,'2014-01-02 04:05:06');
INSERT usuario_has_juegos VALUES (2,15,'2015-11-14 21:22:23');
INSERT usuario_has_juegos VALUES (1,13,'2014-04-07 23:23:56');
INSERT usuario_has_juegos VALUES (1,9,'2013-06-24 01:59:02');
INSERT usuario_has_juegos VALUES (1,7,'2015-10-12 04:59:02');
INSERT usuario_has_juegos VALUES (3,6,'2014-11-12 02:59:02');
INSERT usuario_has_juegos VALUES (3,8,'2015-03-13 05:59:02');
INSERT usuario_has_juegos VALUES (4,12,'2015-02-15 07:59:02');
INSERT usuario_has_juegos VALUES (5,10,'2012-05-25  11:59:02');
INSERT usuario_has_juegos VALUES (5,1,'2015-04-28 13:01:23');
INSERT usuario_has_juegos VALUES (6,12,'2013-10-23 14:15:16');
INSERT usuario_has_juegos VALUES (7,12,'2014-10-11 15:59:45');
INSERT usuario_has_juegos VALUES (7,11,'2014-11-12  20:59:02');
INSERT usuario_has_juegos VALUES (7,4,'2014-12-14 21:23:01');
INSERT usuario_has_juegos VALUES (7,6,'2014-08-08 23:45:22');


/* INSERT TABLA GENEROS */
INSERT genero VALUES (1,'Accion/Aventura');
INSERT genero VALUES (2,'Shooter');
INSERT genero VALUES (3,'RPG');
INSERT genero VALUES (4,'MOBA');
INSERT genero VALUES (5,'Aventura Grafica');
INSERT genero VALUES (6,'Deportes');
INSERT genero VALUES (7,'Estrategia');
INSERT genero VALUES (8,'Musical');
INSERT genero VALUES (9,'Plataformas');

/* INSERT TABLA JUEGOS_HAS_GENEROS */
INSERT juegos_has_generos VALUES (1,1);
INSERT juegos_has_generos VALUES (1,3);
INSERT juegos_has_generos VALUES (2,1);
INSERT juegos_has_generos VALUES (2,2);
INSERT juegos_has_generos VALUES (3,6);
INSERT juegos_has_generos VALUES (4,1);
INSERT juegos_has_generos VALUES (4,2);
INSERT juegos_has_generos VALUES (4,3);
INSERT juegos_has_generos VALUES (5,4);
INSERT juegos_has_generos VALUES (6,8);
INSERT juegos_has_generos VALUES (7,1);
INSERT juegos_has_generos VALUES (8,1);
INSERT juegos_has_generos VALUES (8,3);
INSERT juegos_has_generos VALUES (9,5);
INSERT juegos_has_generos VALUES (10,1);
INSERT juegos_has_generos VALUES (10,2);
INSERT juegos_has_generos VALUES (10,3);
INSERT juegos_has_generos VALUES (11,9);
INSERT juegos_has_generos VALUES (12,5);
INSERT juegos_has_generos VALUES (13,1);
INSERT juegos_has_generos VALUES (13,2);
INSERT juegos_has_generos VALUES (14,7);
INSERT juegos_has_generos VALUES (15,1);



