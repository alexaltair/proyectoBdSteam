<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@page import="mipk.beanDB"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Steam</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="top-number"><p> PROYECTO BD ---> Alejandro Avalos Moreno </p></div>
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="https://www.facebook.com/Steam/"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/steam_games?lang=es"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Index.jsp"><img src="images/logo.jpg" widht="100" height="80" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="Index.jsp">Inicio</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultas Usuarios <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="general.jsp">Info Usuarios</a></li>
                                <li><a href="ListaNumJuegosUsu.jsp">Usuario Num Juegos</a></li>
                                <li><a href="UsuariosCreadosAnios.jsp">Info Usuarios/a�o</a></li>
                                <li><a href="Nivel.jsp">Info seg�n nivel</a></li>
                                <li><a href="NumAmigosGrupos.jsp">Amigos/Grupos Usuarios</a></li>
                                <li><a href="InsertUsuarios.jsp">Crear Usuarios</a></li>
                                <li><a href="UpdateUsuarios.jsp">Actualizar Usuarios</a></li>
                                <li><a href="UsuariosGrupos.jsp">Grupos de usuarios</a></li>
                            </ul>
                        </li>
          
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultas Juegos <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="generalJuegos.jsp">Info juegos</a></li>
                                <li><a href="Buscador.jsp">Buscador</a></li>
                                <li><a href="Categorias.jsp">Categorias</a></li>
                                <li><a href="InsertJuegos.jsp">A�adir Juegos</a></li>
                                <li><a href="JuegosAnios.jsp">Juegos segun A�o</a></li>
                            </ul>
                        </li>
                                             
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->

    <section id="services" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown">
                <h2>Formulario actualizacion usuarios</h2>
                <p class="lead">Formulario que nos permite actualizar los datos de usuarios ya creados.</p>
            </div>
            
            <form class="form-horizontal" role="form" action="UpdateUsuarios.jsp"
					method="get">
					<%
						beanDB basededatos = new beanDB ();
						String consulta = "select idUsuario, nombreUsuario from usuario Order by nombreUsuario";
						String[][] tablares = basededatos.resConsultaSelectA3(consulta);
					%>

					<div>
						<select "data-rel="chosen" name="idUsuario" onchange="submit();">
							<option>Seleccione un nombre de usuario...</option>

							<%
								if (tablares != null)
									for (int i = 0; i < tablares.length; i++) {
							%>
							<option value="<%=tablares[i][0]%>"><%=tablares[i][1]%>
							</option>
							<%
								}
							%>
						</select>
						<%
							
						%>
					</div>
				</form>


				<%
					String idUsuario = "";
					String nombre = "";
					
					try {
						idUsuario = request.getParameter("idUsuario").toString();

					} catch (Exception e) {
					}
					try {
						nombre = request.getParameter("nombreUsu").toString();

					} catch (Exception e) {
					}
					
					if (!idUsuario.equals("")) {
						String consulta2 = "select * from usuario where idUsuario = " + idUsuario;
						String[][] tablares2 = basededatos.resConsultaSelectA3(consulta2);
				%>
				<div>

					<form class="form-horizontal" role="form" action="UpdateUsuarios.jsp"
						method="get">

						<div class="form-group has-error">
							<label class="control-label">Usuario</label> <input type="text"
								class="form-control" name="nombreUsu" value="<%=tablares2[0][1]%>" />
							<input name="id" type="hidden" value="<%=idUsuario%>" /> 
							
						</div>
						<div class="form-group has-error">
							<label class="control-label">Password</label> <input
								name="password" type="text" value="<%=tablares2[0][2]%>" />
						</div>
						<br>
						<div class="form-group has-error">
							<label class="control-label">Nombre Real</label> <input
								name="nombreReal" value="<%=tablares2[0][3]%>" />
						</div>
						<br>
						<div class="form-group has-error">
							<label class="control-label">Apellidos</label> <input
								name="apellidos" value="<%=tablares2[0][4]%>" />
						</div>
						
						<div class="form-group has-error">
							<label class="control-label">Nivel</label> <input
								name="nivel" value="<%=tablares2[0][5]%>" />
						</div>
						
						<div class="form-group has-error">
							<label class="control-label">Provincia</label> <input
								name="provincia" value="<%=tablares2[0][6]%>" />
						</div>
						
						<div class="form-group has-error">
							<label class="control-label">Pais</label> <input
								name="pais" value="<%=tablares2[0][7]%>" />
						</div>
						
						<br>
						
						
						<div style="text-align: center; justify-content: center; display: flex;">
							<input type="submit" value="Actualizar Usuario"/>
						</div>
				
				</form>
			</div>

			</div>
			<%
				}
				if (!nombre.equals("")) {
					String nombreUsu = "";
					String password = "";
					String nombreReal = "";
					String apellidos = "";
					String nivel = "";
					String provincia ="";
					String pais = "";
					String id = "";
					try {
						nombreUsu = request.getParameter("nombreUsu");
						password = request.getParameter("password");
						nombreReal = request.getParameter("nombreReal");
						apellidos = request.getParameter("apellidos");
						nivel = request.getParameter("nivel");
						provincia = request.getParameter("provincia");
						pais = request.getParameter("pais");
						id = request.getParameter("id");

						String consultaFinal = "update usuario set nombreUsuario = '" + nombreUsu + "', password = '"
								+ password + "', nombreReal = '" + nombreReal + "', apellidos = '" + apellidos
								+ "', nivel = '" + nivel + "', provincia = '" + provincia + "', pais = '"
								+ pais + "'where idUsuario = '" + id + "'";
						basededatos.update(consultaFinal);
						System.out.println(consultaFinal);
			%>
		</div>
		<div class="col-md-6"
			style="text-align: center; justify-content: center;">
			<%
				if (nombreUsu.equals("") || password.equals("") || nombreReal.equals("") || apellidos.equals("")
								|| nivel.equals("") || provincia.equals("") || pais.equals("") ) {
			%>
			<label style="font-color: #a94442">No se ha podido actualizar los datos del 
			alumno </label>
			<%
				} else {
			%>
			<b>Los datos del alumno han sido actualizados correctamente</b>
			<%
				}

					} catch (Exception e) {
						System.out.print(e.getMessage());
					}
				}
			%>
		</div>
         
        </div><!--/.container-->
    </section><!--/#services-->


    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2016 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">Steam (Alejandro Avalos Moreno)</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Ir arriba</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>


</body>
</html>